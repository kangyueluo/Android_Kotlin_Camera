package com.example.testing.android_camera

import android.Manifest
import android.app.Activity
import android.graphics.SurfaceTexture
import android.hardware.Camera
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.TextureView
import android.widget.Toast
import com.anthonycr.grant.PermissionsResultAction
import android.Manifest.permission
import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import android.widget.Button
import com.anthonycr.grant.PermissionsManager
import java.io.ByteArrayInputStream
import java.io.File
import java.io.FileOutputStream
import java.lang.reflect.Parameter

import java.util.*
import android.support.v4.view.ViewCompat.setRotation
import android.support.v4.view.ViewCompat.setRotation
import android.hardware.Camera.CameraInfo
import android.os.Environment
import android.view.View
import java.io.IOException


class MainActivity : AppCompatActivity(), TextureView.SurfaceTextureListener, Camera.PictureCallback {

    private val TAG: String = "MainActivity"
    private val permissions = arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
    private var camera: Camera? = null
    private var btnCapture: Button? = null
    private var textureView: TextureView? = null
    private var surface: SurfaceTexture? = null

    private fun askPermission(activity: MainActivity, permissions: Array<String>) {
        PermissionsManager.getInstance().requestPermissionsIfNecessaryForResult(activity,
                permissions, object : PermissionsResultAction() {

            override fun onGranted() {
                for (i in permissions) {
                    Log.d(TAG, i + " Granted")

                }
            }

            override fun onDenied(permission: String) {
                for (i in permissions) {
                    Log.d(TAG, i + " Denied")
                }
            }
        })
    }


    private fun initView() {
        btnCapture = findViewById(R.id.btn_capture);
        // 按鈕點擊事件
        btnCapture!!.setOnClickListener {
            camera!!.takePicture(null, null, this)
        }

        textureView = findViewById(R.id.textureView);
        textureView!!.setSurfaceTextureListener(this);
        surface = textureView!!.getSurfaceTexture();

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        askPermission(this, permissions)
        initView()

    }

    override fun onStop() {
        super.onStop()
    }


    override fun onPictureTaken(data: ByteArray?, camera: Camera?) {

        try {

            val stream: ByteArrayInputStream? = ByteArrayInputStream(data)
            val bitmap: Bitmap = BitmapFactory.decodeStream(stream)
            stream!!.close()

            Log.d(TAG,Environment.getExternalStorageDirectory().absolutePath+"/DCIM/"+Date().getTime()+".png")
            val fileOutputStream: FileOutputStream = FileOutputStream(File(Environment.getExternalStorageDirectory().absolutePath+"/DCIM/"+Date().getTime()+".png"))


            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, fileOutputStream);


            // 寫入檔案
            fileOutputStream.flush();
            fileOutputStream.close();

            camera!!.startPreview();

        } catch (e: IOException) {
            Log.i("Tack_IOException", e.message);
        }

    }

    override fun onSurfaceTextureAvailable(surface: SurfaceTexture?, width: Int, height: Int) {
        try {
            camera = Camera.open(CameraInfo.CAMERA_FACING_BACK)
            // 取得相機參數
            val parameters = camera!!.getParameters()

            // 關閉閃光燈
            parameters.flashMode = Camera.Parameters.FLASH_MODE_OFF

            // 設定最佳預覽尺寸
            var listPreview = parameters.supportedPreviewSizes
            parameters.setPreviewSize(listPreview!![0].width, listPreview[0].height)
            listPreview = null

            // 設定最佳照片尺寸
            var listPicture = parameters.supportedPictureSizes
            parameters.setPictureSize(listPicture!![0].width, listPicture[0].height)
            listPicture = null

            // 設定照片輸出為90度
            parameters.setRotation(90)

            // 設定預覽畫面為90度
            camera!!.setDisplayOrientation(90)

            // 設定相機參數
            camera!!.setParameters(parameters)

            // 設定顯示的Surface
            camera!!.setPreviewTexture(surface)
            // 開始顯示
            camera!!.startPreview()
        } catch (e: IOException) {
        }
    }


    override fun onSurfaceTextureUpdated(p0: SurfaceTexture?) {

    }

    override fun onSurfaceTextureDestroyed(surface: SurfaceTexture?): Boolean {
        if (camera != null) {
            camera!!.stopPreview();
            camera!!.release();
        }
        return true;
    }


    override fun onSurfaceTextureSizeChanged(p0: SurfaceTexture?, p1: Int, p2: Int) {

    }


}
